﻿using System.ComponentModel.DataAnnotations;
using Arrba.Domain.Models;
using Newtonsoft.Json;

namespace Arrba.DTO
{
    public class OrderItemDto
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("checked")]
        public bool Checked { get; set; }
        [JsonProperty("count")]
        public int Count { get; set; }
        [JsonProperty("item")]
        public AdVehicle Item { get; set; }
    }
}
