﻿using System.Collections.Generic;

namespace Arrba.DTO
{
    public class CategoryDto
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string NameSingular { get; set; }
        public string Alias { get; set; }
        public long ParentID { get; set; }
        public bool HideModelField { get; set; }
        public bool HideYearField { get; set; }
        public bool HideTitleField { get; set; }
        public string TitlePlaceholder { get; set; }
        public ICollection<CategoryDto> Childrens;
    }
}
