﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Arrba.DTO
{
    public class OrderCreateDto
    {
        [JsonProperty("deliveryPartnerId")]
        public long DeliveryPartnerId { get; set; }
        public string Number { get; set; }
        [Required]
        [JsonProperty("address")]
        public string Address { get; set; }
        [Required]
        [JsonProperty("phone")]
        public string Phone { get; set; }
        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("surname")]
        public string Surname { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("comment")]
        public string Comment { get; set; }
        public DateTime AddDate { get; set; }
        [Required]
        [JsonProperty("grecaptchaToken")]
        private string GrecaptchaToken { get; set; }
        [Required]
        [JsonProperty("items")]
        public OrderItemDto[] ItemOrders { get; set; }
    }
}
