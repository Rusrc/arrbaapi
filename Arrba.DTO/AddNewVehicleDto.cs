﻿using System.ComponentModel.DataAnnotations;
using Arrba.Domain.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Arrba.DTO
{
    public class AddNewVehicleDto
    {
        [Required]
        [JsonProperty("superCategoryId")]
        public long SuperCategoryId { get; set; }
        [Required]
        [JsonProperty("categoryId")]
        public long CategoryId { get; set; }
        [JsonProperty("TypeId")]
        public long? TypeId { get; set; }
        [JsonProperty("BrandId")]
        public long? BrandId { get; set; }
        [JsonProperty("ModelId")]
        public long? ModelId { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("modelValue")]
        public string ModelValue { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [Required]
        [JsonProperty("CurrencyId")]
        public long CurrencyId { get; set; }
        [Required]
        [JsonProperty("CountryId")]
        public long CountryId { get; set; }
        [Required]
        [JsonProperty("CityId")]
        public long CityId { get; set; }
        [JsonProperty("additionalComment")]
        public string AdditionalComment { get; set; }
        [JsonProperty("commentMode")]
        public string CommentMode { get; set; }
        [Required]
        [JsonProperty("price")]
        public double Price { get; set; }
        [JsonProperty("minimalPrice")]
        public double? MinimalPrice { get; set; }
        [JsonProperty("year")]
        public string Year { get; set; }
        [JsonProperty("phoneNumbers")]
        public string[] PhoneNumbers { get; set; }
        [JsonProperty("properties")]
        public JObject Properties { get; set; }
        [JsonProperty("temporaryImageFolder")]
        public string TemporaryImageFolder { get; set; }
        [JsonProperty("dealershipId")]
        public long? DealershipId { get; set; }
        [JsonProperty("mapJsonCoord")]
        public string MapJsonCoord { get; set; }
        [JsonProperty("itemInfoQuantity")]
        public int? ItemInfoQuantity { get; set; }
        [JsonProperty("itemInfoWeight")]
        public int? ItemInfoWeight { get; set; }
        [JsonProperty("instalmentSelling")]
        public bool InstalmentSelling { get; set; }
        [JsonProperty("customsCleared")]
        public bool CustomsCleared { get; set; }
        [JsonProperty("hotSelling")]
        public bool HotSelling { get; set; }
        [JsonProperty("exchangePossible")]
        public bool ExchangePossible { get; set; }

        public VehicleCondition Condition { get; set; }

        public bool IsDealer => DealershipId.HasValue;
    }
}
