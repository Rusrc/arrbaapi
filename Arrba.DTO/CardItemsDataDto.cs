﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Arrba.DTO
{
    public class CardItemsDataDto
    {
        [JsonProperty("items")]
        public IEnumerable<VehicleDto> Items { get; set; }
        [JsonProperty("pagination")]
        public PaginationDto Pagination { get; set; }
        [JsonProperty("categoryId")]
        public long CategoryId { get; set; }
    }
}
