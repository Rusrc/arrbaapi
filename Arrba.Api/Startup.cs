﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Compression;
using System.Text.RegularExpressions;

using Arrba.Api.ConfigurationExtensions;
using Arrba.Domain;
using Arrba.Extensions;
using Arrba.ImageLibrary;
using Arrba.Middleware;
using Arrba.Repositories;
using Arrba.Repositories.EntityFramework.PostgreSQL;
using Arrba.Services;
using Arrba.Services.Configuration;
using Arrba.Services.Decline;
using Arrba.Services.Logger;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using HttpHeaders = Microsoft.Net.Http.Headers;

namespace Arrba.Api
{
    /// <summary>
    /// Startup class
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Startup
        /// </summary>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// ConfigureServices
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Latest)
                .AddNewtonsoftJson(options =>
             {
                 options.SerializerSettings.ContractResolver = new DefaultContractResolver();
             });

            // .NET Core 3.1
            services.AddControllers(options => {
                options.EnableEndpointRouting = false;
                options.Conventions.Add(new RouteTokenTransformerConvention(new LowerCaseParameterTransformer()));
            });
                // .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNameCaseInsensitive = false);

            services.AddScoped<ILogService>(s => new LogService("Arrba.API"));
            services.AddSingleton<IDeclineService, RussianDeclineService>();

            services.AddDbArrbaContext(Configuration, false);
            services.AddIdentityForDbArrbaContext();
            services.AddJwtAuthentication();
            services.AddSwaggerDocumentation();
            services.AddAutoMapper(typeof(Startup));

            services.AddTransient<ApplicationConfiguration>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<ICaptchaService, GrecaptchaService>();

            var provider = services.BuildServiceProvider();
            var env = provider.GetService<IWebHostEnvironment>();

            // services.AddTransient(s => new ImgManager(env.WebRootPath));
            // services.AddTransient<IImgManager, ImgManagerFirebase>();

            services.AddTransient<IImgManager, ImgManagerSkiaSharp>(
                s => new ImgManagerSkiaSharp(
                    env.WebRootPath,
                    provider.GetService<ApplicationConfiguration>(),
                    provider.GetService<ILogService>()));

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new List<CultureInfo>
                {
                   new CultureInfo("ru-RU"),
                   new CultureInfo("ru"),
                   //new CultureInfo("en-US"),
                   //new CultureInfo("en"),
                };

                options.DefaultRequestCulture = new RequestCulture("ru-RU");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

            if (!env.IsDevelopment())
            {
                services.AddHsts(options =>
                {
                    options.Preload = true;
                    options.IncludeSubDomains = true;
                    options.MaxAge = TimeSpan.FromDays(60);
                });

                services.AddResponseCompression();
                services.Configure<GzipCompressionProviderOptions>
                    (options =>
                    {
                        options.Level = CompressionLevel.Fastest;
                    });
            }

            // TODO maybe it is unnecessary
            // https://docs.microsoft.com/ru-ru/aspnet/core/performance/caching/middleware?view=aspnetcore-2.2
            services.AddResponseCaching(options =>
            {
                options.UseCaseSensitivePaths = true;
                // options.MaximumBodySize = 1024 * 2;
            });

            services.AddArrbaCors();
        }

        /// <summary>
        /// Configre
        /// </summary>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, DbArrbaContext context)
        {
            // TODO Check is it used on release mode
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                // app.UseHttpsRedirection();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            if (!context.AllMigrationsApplied())
            {
                context.Database.Migrate();
            }

            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseSwaggerDocumentation();
            app.UseStaticFiles();
            // https://docs.microsoft.com/ru-ru/aspnet/core/fundamentals/portable-object-localization?view=aspnetcore-2.2
            app.UseRequestLocalization();
            // TODO maybe it is unnecessary
            // https://docs.microsoft.com/ru-ru/aspnet/core/performance/caching/middleware?view=aspnetcore-2.2
            app.UseResponseCaching();
            app.Use(async (httpContext, next) =>
            {
                var headers = httpContext.Response.GetTypedHeaders();

                headers.CacheControl = new HttpHeaders.CacheControlHeaderValue
                {
                    Public = true,
                    MaxAge = TimeSpan.FromSeconds(60)
                };

                httpContext.Response.Headers[HttpHeaders.HeaderNames.Vary] = new[] { "Accept-Encoding" };

                await next();
            });

            app.UseCors("_specificOriginsForArrba");

            // .NET Core 3.1

            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Make all routing parameters in lower case
        /// </summary>
        private class LowerCaseParameterTransformer : IOutboundParameterTransformer
        {
            public string TransformOutbound(object value)
            {
                if(value == null)
                {
                    return null;
                }
                
                return Regex.Replace(value.ToString(), "([a-z])([A-Z])", "$1$2").ToLower();
            }
        }
    }


}
