﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arrba.DTO;
using Arrba.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Arrba.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrderController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        [HttpPost]
        [Route("create", Name = "Post order")]
        public async Task<ActionResult<IEnumerable<VehicleDto>>> Post(OrderCreateDto order)
        {



            return Ok();
        }
    }
}
