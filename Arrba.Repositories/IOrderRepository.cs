﻿using System;
using System.Collections.Generic;
using System.Text;
using Arrba.Domain.Models;

namespace Arrba.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}
