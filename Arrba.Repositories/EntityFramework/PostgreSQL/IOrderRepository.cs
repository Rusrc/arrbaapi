﻿using System;
using System.Collections.Generic;
using System.Text;
using Arrba.Domain;
using Arrba.Domain.Models;

namespace Arrba.Repositories.EntityFramework.PostgreSQL
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(DbArrbaContext context) : base(context)
        {
        }
    }
}
