﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Arrba.Domain;
using Arrba.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Arrba.Repositories.EntityFramework.PostgreSQL
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(DbArrbaContext context) : base(context)
        {
        }

        public virtual async Task<Category> GetAsync(string alias)
        {
            return await _context.Categories.FirstOrDefaultAsync(c => c.Alias == alias);
        }

        public override async Task<IEnumerable<Category>> GetAllAsync(Expression<Func<Category, bool>> predicate)
        {
            return await _context
                .Categories
                .Include(c => c.Parent)
                .Where(predicate)
                .ToListAsync();
        }

        public IQueryable<Category> GetCategories()
        {
            return _context.Categories
                .Include(c => c.Parent);
        }
    }
}
