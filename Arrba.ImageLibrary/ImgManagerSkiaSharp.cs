﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Arrba.Exceptions;
using Arrba.ImageLibrary.Json;
using Arrba.ImageLibrary.ModelViews;
using Arrba.Services.Configuration;
using Arrba.Services.Logger;
using Firebase.Storage;
using SkiaSharp;
using static Arrba.Constants.CONSTANT;


namespace Arrba.ImageLibrary
{
    public class ImgManagerSkiaSharp : IImgManager
    {
        private readonly ApplicationConfiguration _applicationConfiguration;

        private const int Quality = 80;

        private readonly (int Width, int Height, bool Main)[] _sizes = {
            (Width: 1920, Height: 1080, Main: true),
            (Width: 540, Height: 405, Main: false) // MIDDLE_FILE_NAME_PREFIX
        };

        private readonly string _wwwRootPath;
        private readonly ILogService _logService;

        private readonly string _logoPath;
        private readonly string _rootUploadFolder;
        private readonly string _backgroundImagePath;

        public ImgManagerSkiaSharp(
            string wwwRootPath,
            ApplicationConfiguration applicationConfiguration,
            ILogService logService)
        {
            _wwwRootPath = wwwRootPath;
            _applicationConfiguration = applicationConfiguration;
            _logService = logService;

            _rootUploadFolder = ImgFolderManager.GetRootUploadFolderPath(_wwwRootPath);
            _logoPath = ImgFolderManager.GetLogoPath(_wwwRootPath);
            _backgroundImagePath = ImgFolderManager.GetBackgroundImagePath(_wwwRootPath);
        }

        /// <summary>
        /// Save images on disk
        /// Then upload images on storage
        /// Delete all images on disk
        /// </summary>
        /// <param name="folderCategoryPath"></param>
        /// <param name="uniqueImgFolderName"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<ImgJson> SaveImagesAsync(string folderCategoryPath, string uniqueImgFolderName, int limit = 5)
        {
            ImgJson imgJson;
            if (string.IsNullOrEmpty(uniqueImgFolderName))
            {
                return ImgJson.Default;
            }

            string targetFolder = Path.Combine(_rootUploadFolder, folderCategoryPath, uniqueImgFolderName);
            string sourcePath = ImgFolderManager.GetTempUniqueItemFolder(uniqueImgFolderName, _wwwRootPath);

            try
            {
                // save images on disk
                imgJson = await Task.Run(() => this.SaveImages(sourcePath, targetFolder));

                // post images into bitbuket
                await this.SaveImagesOnStorageAsync(targetFolder, uniqueImgFolderName);
            }
            finally
            {
                var t1 = Task.Factory.StartNew(() => ImgFolderManager.DeleteFolderOfTempFiles(_wwwRootPath, uniqueImgFolderName));
                var t2 = Task.Factory.StartNew(() => ImgFolderManager.DeleteFolderOfCroppedFiles(_wwwRootPath, folderCategoryPath, uniqueImgFolderName));
            }

            return imgJson;
        }

        /// <summary>
        /// Return image pathes generated form disk
        /// {
        ///     FullFileName = "{ROOT_HOST_HTTPS}/{UPLOADS_FOLDER}/{categoryId}/{folderName}/{indexName}-{BIG_FILE_NAME_PREFIX}.jpg",
        ///     MiddleFileName = "{ROOT_HOST_HTTPS}/{UPLOADS_FOLDER}/{categoryId}/{folderName}/{indexName}-{MIDDLE_FILE_NAME_PREFIX}.jpg",
        ///     SmallFileName = "{ROOT_HOST_HTTPS}/{UPLOADS_FOLDER}/{categoryId}/{folderName}/{indexName}-{MIDDLE_FILE_NAME_PREFIX}.jpg",
        ///     SuperSmallFileName =$"{ROOT_HOST_HTTPS}/{UPLOADS_FOLDER}/{categoryId}/{folderName}/{indexName}-{MIDDLE_FILE_NAME_PREFIX}.jpg",
        /// }
        /// </summary>
        /// <param name="imgJsonString"></param>
        /// <param name="folderName"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static IEnumerable<AdImgDto> GetImagesFromDisk(string imgJsonString, string folderName, string categoryId)
        {
            if (string.IsNullOrEmpty(imgJsonString))
            {
                throw new BusinessLogicException("Hey! JsonList must be not null");
            }

            var imgJson = ImgJson.Parse(imgJsonString);
            var adImgList = new List<AdImgDto>();

            imgJson.ImgJsonOrderList.ForEach(e =>
            {
                var indexName = e.IndexName.ToString();
                adImgList.Add(new AdImgDto
                {
                    FullFileName = $"${ROOT_HOST_HTTPS}/{UPLOADS_FOLDER}/{categoryId}/{folderName}/{indexName}-{BIG_FILE_NAME_PREFIX}.jpg",
                    MiddleFileName = $"{ROOT_HOST_HTTPS}/{UPLOADS_FOLDER}/{categoryId}/{folderName}/{indexName}-{MIDDLE_FILE_NAME_PREFIX}.jpg",
                    SmallFileName = $"{ROOT_HOST_HTTPS}/{UPLOADS_FOLDER}/{categoryId}/{folderName}/{indexName}-{MIDDLE_FILE_NAME_PREFIX}.jpg",
                    SuperSmallFileName = $"{ROOT_HOST_HTTPS}/{UPLOADS_FOLDER}/{categoryId}/{folderName}/{indexName}-{MIDDLE_FILE_NAME_PREFIX}.jpg",
                    ImageStatus = e.Order == 0 ? AdImageStatus.Main : AdImageStatus.NotMain
                });
            });

            return adImgList;
        }

        /// <summary>
        /// Return image pathes generated form remove storage
        /// {
        ///     FullFileName = string.Format(FIREBASE_URL, $"thumb%40{FULL_FILE_NAME_PREFIX}-{folderName}_{indexName}.{ext}"),
        ///     MiddleFileName = string.Format(FIREBASE_URL, $"thumb%40{MIDDLE_FILE_NAME_PREFIX2}-{folderName}_{indexName}.{ext}"),
        ///     SmallFileName = string.Format(FIREBASE_URL, $"thumb%40{MIDDLE_FILE_NAME_PREFIX2}-{folderName}_{indexName}.{ext}"),
        ///     SuperSmallFileName = string.Format(FIREBASE_URL, $"thumb%40{MIDDLE_FILE_NAME_PREFIX2}-{folderName}_{indexName}.{ext}"),
        /// }
        /// </summary>
        /// <param name="imgJsonString"></param>
        /// <param name="folderName"></param>
        /// <param name="_"></param>
        /// <returns></returns>
        public static IEnumerable<AdImgDto> GetImagesFromStorage(string imgJsonString, string folderName, string _)
        {
            if (string.IsNullOrEmpty(imgJsonString))
            {
                throw new BusinessCriticalLogicException("Hey! JsonList must be not null");
            }

            var imgJson = ImgJson.Parse(imgJsonString);
            var adImgList = new List<AdImgDto>();
            const string ext = "jpg";

            // thumb@1024x720-02f78438329c484a9f50035d32e8a08f_15.jpg
            imgJson.ImgJsonOrderList.ForEach(e =>
            {
                var indexName = e.IndexName.ToString();
                adImgList.Add(new AdImgDto
                {
                    FullFileName = string.Format(FIREBASE_URL, $"{folderName}-{indexName}.{ext}"),
                    MiddleFileName = string.Format(FIREBASE_URL, $"{folderName}-{indexName}-{MIDDLE_FILE_NAME_PREFIX2}.{ext}"),
                    SmallFileName = string.Format(FIREBASE_URL, $"{folderName}-{indexName}-{MIDDLE_FILE_NAME_PREFIX2}.{ext}"),
                    SuperSmallFileName = string.Format(FIREBASE_URL, $"{folderName}-{indexName}-{MIDDLE_FILE_NAME_PREFIX2}.{ext}"),
                    ImageStatus = e.Order == 0 ? AdImageStatus.Main : AdImageStatus.NotMain
                });
            });

            return adImgList;
        }

        #region Helpers
        private ImgJsonOrder GetSaveImagesAndImgJsonOrder(string tempFilePath, string userFolderPath, int indexName)
        {
            SKBitmap bitmap = null;
            try
            {
                var sw = Stopwatch.StartNew();
                bitmap = SKBitmap.Decode(tempFilePath);

                foreach (var size in _sizes)
                {
                    string pathToSaveJpg;

                    if (size.Main)
                        pathToSaveJpg = Path.Combine(userFolderPath, $"{indexName}.jpg");
                    else
                        pathToSaveJpg = Path.Combine(userFolderPath, $"{indexName}-{size.Width}x{size.Height}.jpg");

                    // Compute index to get new width
                    var indexWidth = bitmap.Width / (decimal)bitmap.Height;
                    var newWidth = Convert.ToInt32(Math.Round(size.Height * indexWidth, 3, MidpointRounding.AwayFromZero));

                    bitmap = bitmap.Resize(new SKImageInfo(newWidth, size.Height), SKFilterQuality.Medium);

                    if (!size.Main)
                    {
                        bitmap = ScaledBitmap(bitmap, size.Height, size.Width);
                    }

                    using (var image = SKImage.FromBitmap(bitmap))
                    using (var data = image.Encode(SKEncodedImageFormat.Jpeg, Quality))
                    using (var stream = File.OpenWrite(pathToSaveJpg))
                        data.SaveTo(stream);
                }

                sw.Stop();
                _logService.Info(
                    $@"The file with index '{indexName}' croped on {_sizes.Length} sizes for {sw.ElapsedMilliseconds} ms. tempFilePath: {tempFilePath}");

                return new ImgJsonOrder { Order = indexName, IndexName = indexName };
            }
            catch (Exception ex)
            {
                throw new BusinessCriticalLogicException(ex.Message, ex);
            }
            finally
            {
                bitmap?.Dispose();
            }
        }

        private ImgJson SaveImages(string sourcePath, string targetFolder, int limit = 5)
        {
            string[] tempFilePaths = Directory.GetFiles(sourcePath);
            if (tempFilePaths.Length > limit)
            {
                tempFilePaths = tempFilePaths.Take(limit).ToArray();
            }

            string userFolderPath = ImgManager.CheckFolderAndCreate(targetFolder, true);

            var imgJson = ImgJson.Default;

            Parallel.ForEach(tempFilePaths, (tempFilePath, pls, index) =>
            {
                var imgJsonOrder = GetSaveImagesAndImgJsonOrder(tempFilePath, userFolderPath, (int)index);
                imgJson
                    .ImgJsonOrderList
                    .Add(imgJsonOrder);
            });


            imgJson.CountHistory = imgJson.GetMaxIndexNameInList();

            return imgJson;
        }

        private async Task SaveImagesOnStorageAsync(string croppedFilesPath, string uniqueImgFolderName)
        {
            string[] filePaths = Directory.GetFiles(croppedFilesPath);

            foreach (var filePath in filePaths)
            {
                var stream = File.Open(filePath, FileMode.Open);
                var fileUniquePrefix = uniqueImgFolderName;
                var fileName = Path.GetFileNameWithoutExtension(filePath);
                var fileExtension = Path.GetExtension(filePath);
                // d7c4f3151de64646a25390056c3fe10b-0-400x300.jpg
                var newFileName = $"{fileUniquePrefix}-{fileName}{fileExtension}";

                try
                {
                    var imageUrl = await new FirebaseStorage(_applicationConfiguration.FirebaseStorageEndpoint)
                        .Child("images")
                        .Child(newFileName)
                        .PutAsync(stream);

                    _logService.Info($"Image {newFileName} added successfully.... url {imageUrl}");
                }
                catch (Exception ex)
                {
                    _logService.Error($"Image {newFileName} failed.... " + ex.Message, ex);
                    throw;
                }
                finally
                {
                    stream?.Dispose();
                }
            }
        }

        private int GetIndexName(string fileName)
        {
            var test = new Regex("^(\\d+)-\\d+x\\d+$|^(\\d+)$", RegexOptions.IgnoreCase);
            var get = new Regex("^(?<indexName>\\d+)", RegexOptions.IgnoreCase);

            string indexName = get.Match(fileName).Groups["indexName"].Value;

            if (test.IsMatch(fileName) && int.TryParse(indexName, out var value))
            {
                return value;
            }

            throw new BusinessLogicException($"fileName `{fileName}` is wrong");
        }

        private SKRectI GetCroppedRectI(int originalWidth, int originalHeight, int containerWidth, int containerHeight)
        {
            var newWidth = originalWidth;
            var newHeight = originalHeight;

            double relX = newWidth / (double)containerWidth;
            double relY = newHeight / (double)containerHeight;

            int dX = 0;
            int dY = 0;

            if (relX >= relY && relY > 1.0)
            {
                newHeight = containerHeight;
                newWidth = relX > relY ? (int)(newWidth / relY) : containerWidth;
            }
            else if (relY > relX && relX > 1.0)
            {
                newWidth = containerWidth;
                newHeight = (int)(newHeight / relX);
            }

            dX = (int)((containerWidth - newWidth) / 2.0);
            dY = (int)((containerHeight - newHeight) / 2.0);

            if (dX < 0 || dY < 0)
            {
                int dX1 = 0;
                int dY1 = 0;

                if (dX < 0)
                {
                    dX1 = dX * -1;
                    dX = 0;
                }
                if (dY < 0)
                {
                    dY1 = dY * (-1);
                    dY = 0;
                }

                newWidth = newWidth - dX1 * 2;
                newHeight = newHeight - dY1 * 2;

                return new SKRectI(left: dX1, top: dY1, right: newWidth, bottom: newHeight);
            }

            return new SKRectI(left: dX, top: dY, right: newWidth, bottom: newHeight);
        }

        public SKBitmap ScaledBitmap(SKBitmap bitmap, int desiredHeight, int desiredWidth)
        {
            // Calculate the scale
            // Maintain aspect ratio
            double scale = 1.0;
            double height = desiredHeight;
            double width = desiredWidth;

            // Which is scaled more, height or width?
            if (desiredWidth / bitmap.Width < desiredHeight / bitmap.Height)
            {
                scale = desiredWidth / (double)bitmap.Width;
                height = desiredHeight * scale;
            }
            else
            {
                scale = (double)desiredHeight / bitmap.Height;
                width = (double)desiredWidth * scale;
            }

            Point centreoffset = new Point((int)(desiredWidth - width) / 2, (int)(desiredHeight - height) / 2);

            SKBitmap croppedBitmap = new SKBitmap(desiredWidth, desiredHeight);
            using (SKCanvas canvas = new SKCanvas(croppedBitmap))
            {
                // Draw back buffer
                canvas.Clear(SKColors.Transparent);
                canvas.DrawBitmap(bitmap, new SKRect(0, 0, bitmap.Width, bitmap.Height), new SKRect(centreoffset.X, centreoffset.Y, centreoffset.X + (int)width, centreoffset.Y + (int)height));
            }

            return croppedBitmap;
        }
        #endregion
    }
}
