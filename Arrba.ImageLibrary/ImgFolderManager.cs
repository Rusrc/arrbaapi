﻿using Arrba.Constants;
using Arrba.ImageLibrary.ModelViews;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ImgJson = Arrba.ImageLibrary.Json.ImgJson;
using ImgJsonOrder = Arrba.ImageLibrary.Json.ImgJsonOrder;
using static Arrba.Constants.CONSTANT;

namespace Arrba.ImageLibrary
{
    public static class ImgFolderManager
    {
        /// <summary>
        /// tempImages
        /// </summary>
        private const string TEMP_IMAGE_FOLDER = "tempImages";

        /// <summary>
        /// Retrun full path of temp unique folder
        /// Example: D:/wwwroot/tempimages/ea762e884c4e4be69eec1b033fb6510c
        /// </summary>
        /// <returns></returns>
        public static DirectoryInfo GenerateTempUniqueItemFolder(string uniqueItemFolder, string wwwrootPath)
        {
            return Directory.CreateDirectory(Path.Combine(wwwrootPath, TEMP_IMAGE_FOLDER, uniqueItemFolder));
        }

        /// <summary>
        /// Retrun full path
        /// D:/App_Data/tempImages/ea762e884c4e4be69eec1b033fb6510c
        /// </summary>
        /// <returns></returns>
        public static string GetTempUniqueItemFolder(string uniqueItemFolder, string wwwrootPath)
        {
            string sourcePath = Path.Combine(wwwrootPath, TEMP_IMAGE_FOLDER, uniqueItemFolder);
            return sourcePath;
        }

        public static void DeleteFolderOfTempFiles(string wwwrootPath, string uniqueItemFolder)
        {
            string sourcePath = Path.Combine(wwwrootPath, TEMP_IMAGE_FOLDER, uniqueItemFolder);
            if (Directory.Exists(sourcePath))
            {
                Directory.Delete(sourcePath, true);
            }
        }

        public static void DeleteFolderOfCroppedFiles(string wwwrootPath, string categoryName, string uniqueItemFolder)
        {
            string targetPath = Path.Combine(wwwrootPath, UPLOADS_FOLDER, categoryName, uniqueItemFolder);
            if (Directory.Exists(targetPath))
            {
                Directory.Delete(targetPath, true);
            }
        }

        /// <summary>
        /// Retrun relative root path
        /// /uploads/1/ea762e884c4e4be69eec1b033fb6510c
        /// </summary>
        /// <returns></returns>
        public static string GetRelativeItemFolderPath(string categoryName, string uniqueItemFolder) => Path.Combine(UPLOADS_FOLDER, categoryName, uniqueItemFolder);

        /// <summary>
        /// Retrun full path
        /// D:/uploads/
        /// </summary>
        /// <returns></returns>
        public static string GetRootUploadFolderPath(string wwwrootPath) => Path.Combine(wwwrootPath, UPLOADS_FOLDER);

        /// <summary>
        /// Retrun relative path
        /// /uploads/no_image.jpg
        /// </summary>
        /// <returns></returns>
        public static string GetRelativeNoImagePath() => Path.Combine("content", SETTINGS.NoImageName);

        /// <summary>
        /// Retrun full path
        /// D:/uploads/no_image.jpg
        /// </summary>
        /// <returns></returns>
        public static string GetNoImagePath(string wwwrootPath) => Path.Combine(wwwrootPath, "content", SETTINGS.NoImageName);

        /// <summary>
        /// Retrun full path
        /// D:/uploads/background_image.jpg
        /// </summary>
        /// <returns></returns>
        public static string GetBackgroundImagePath(string wwwrootPath) => Path.Combine(wwwrootPath, "content", SETTINGS.BackgroundImage);

        /// <summary>
        /// Logo path
        /// </summary>
        /// <param name="wwwrootPath"></param>
        /// <returns></returns>
        public static string GetLogoPath(string wwwrootPath) => Path.Combine(wwwrootPath, "content", SETTINGS.LogoWaterMarkFile);

        /// <summary>
        /// Получает название файлов из списка объектов ImgJsonOrder преобразуя в список объектов типа AdImgModelView 
        /// </summary>
        /// <param name="jsonList">Список сортер полученный из json</param>
        /// <returns>список объектов типа AdImgModelView</returns>
        public static List<AdImgDto> GetImages(List<ImgJsonOrder> jsonList)
        {
            //1024x720
            var adImgList = new List<AdImgDto>();

            jsonList.ForEach(e =>
            {
                var indexName = e.IndexName.ToString();
                adImgList.Add(new AdImgDto
                {
                    FullFileName = string.Concat((object)indexName, FULL_FILE_NAME_PREFIX, ".jpg"),
                    MiddleFileName = string.Concat((object)indexName, MIDDLE_FILE_NAME_PREFIX, ".jpg"),
                    SmallFileName = string.Concat((object)indexName, SMALL_FILE_NAME_PREFIX, ".jpg"),
                    SuperSmallFileName = string.Concat((object)indexName, SUPER_SMALL_FILE_NAME_PREFIX, ".jpg"),
                    ImageStatus = e.Order == 0 ? AdImageStatus.Main : AdImageStatus.NotMain
                });
            });

            return adImgList;
        }

        /// <summary>
        /// Получает название файлов из json строки JsonList преобразуя в список объектов типа AdImgModelView 
        /// </summary>
        /// <param name="jsonList">json строка</param>
        /// <returns></returns>
        public static List<AdImgDto> GetImages(string jsonList)
        {
            if (jsonList == null)
            {
                throw new NullReferenceException("Hey! JsonList must be not null");
            }


            var jsonResult = ImgJson.Parse(jsonList);

            return GetImages(jsonResult.ImgJsonOrderList);
        }

        /// <summary>
        /// Получает название файлов из указанной папки преобразуя в список объектов типа AdImgModelView 
        /// </summary>
        /// <param name="rootFolder">json строка</param>
        /// <param name="folderImgName">json строка</param>
        /// <returns></returns>
        public static List<AdImgDto> GetImages(string rootFolder, string folderImgName)
        {
            var fileFolderName = Path.Combine(rootFolder, folderImgName);

            if (Directory.Exists(fileFolderName))
            {
                var imgFiles = GetFilesName(Directory.GetFiles(fileFolderName));
                var fullImgs = imgFiles.Where(e => e.Contains(CONSTANT.FULL_FILE_NAME_PREFIX)).ToList<string>();
                var adImgList = new List<AdImgDto>();

                fullImgs.ForEach((img) =>
                {
                    var name = new Regex("^[0-9]+", RegexOptions.IgnoreCase).Match(img).Value;

                    adImgList.Add(new AdImgDto
                    {
                        FullFileName = GetFileName(imgFiles, name, CONSTANT.FULL_FILE_NAME_PREFIX), // or if full just "img"
                        MiddleFileName = GetFileName(imgFiles, name, CONSTANT.MIDDLE_FILE_NAME_PREFIX),
                        SmallFileName = GetFileName(imgFiles, name, CONSTANT.SMALL_FILE_NAME_PREFIX),
                        SuperSmallFileName = GetFileName(imgFiles, name, CONSTANT.SUPER_SMALL_FILE_NAME_PREFIX),
                        ImageStatus =
                            new Regex("^0{1}", RegexOptions.IgnoreCase).IsMatch(img)
                                ? AdImageStatus.Main
                                : AdImageStatus.NotMain,
                    });
                });

                return adImgList;
            }

            return new List<AdImgDto> { null };

        }

        #region Helpers
        /// <summary>
        /// Получает имена файлов без пути или с путями
        /// </summary>
        /// <param name="fullPaths"></param>
        /// <param name="thatMatchWith">взять файлы соответсвующие регулярному выражению
        /// елси значение Null, то берет все файлы</param>
        /// <returns></returns>
        private static List<string> GetFilesName(string[] fullPaths, Regex thatMatchWith = null)
        {
            List<string> result = new List<string>();

            if (thatMatchWith != null)
            {
                fullPaths.ToList().ForEach(e =>
                {
                    if (thatMatchWith.Match(e).Success)
                    {
                        result.Add(Path.GetFileName(e));
                    }
                });
            }
            else
            {
                fullPaths.ToList().ForEach(e => result.Add(Path.GetFileName(e)));
            }


            return result;
        }

        /// <summary>
        /// Получает имя файла из списка ImgFiles, где имя начинается с FileNameBase + AdditionalExtenstion
        /// </summary>
        /// <returns>Возврощает имя файла вида FileNameBaseAdditionalExtenstion (e.g. 0-full)</returns>
        private static string GetFileName(IReadOnlyCollection<string> imgFiles, string fileNameBase, string additionalExtenstion)
        {
            if (imgFiles.Any(e => e.Contains(fileNameBase + additionalExtenstion)))
            {
                return imgFiles.SingleOrDefault(e => e.StartsWith(fileNameBase + additionalExtenstion));
            }

            return string.Empty;
        }
        #endregion
    }
}
