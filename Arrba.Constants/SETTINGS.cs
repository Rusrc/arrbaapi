﻿namespace Arrba.Constants
{
    public static partial class SETTINGS
    {
        public static int HotAdLimit => 27;
        /// <summary>
        /// Default rows on page
        /// </summary>
        public static int CountRows => 20;
        /// <summary>
        /// INFO_MAIL = "info@arrba.ru"
        /// </summary>
        public static string MailInfo => "info@arrba.ru";
        /// <summary>
        /// Display the name of email when comming to client, by default 'arrba'
        /// </summary>
        public static string MailDisplayName => "arrba";
        /// <summary>
        /// FROM = "info@arrba.ru";
        /// </summary>
        public static string MailFrom => "info@arrba.ru";
        /// <summary>
        /// PW = "gCpBCLK00N3mCXxdJ9qC";
        /// </summary>
        public static string MailPassword => "gCpBCLK00N3mCXxdJ9qC";
        /// <summary>
        /// Default currency id that matched with currency id in database 
        /// </summary>
        public static long DefaultCurrencyID => 4;
        /// <summary>
        /// Default country Id
        /// 1 - Russia, 2 - Kazahstan
        /// </summary>
        public static long DefaultCountryId => 1;
        /// <summary>
        /// Use distributed cache or not
        /// </summary>
        public static bool UseMemaryCaching => false;
        /// <summary>
        /// Default price for user balance
        /// </summary>
        public static double PromotionPrice => 0;   
        /// <summary>
        /// ~/App_Data/filename.png
        /// </summary>
        public static string NoImageName => "no_img1.jpg";
        public static string LogoWaterMarkFile => "logo_watermark_dark_big3.png";
        public static string BackgroundImage => "background1.png";
    } 
}
