﻿using System.ComponentModel.DataAnnotations;
using Arrba.Domain.Models;
using Microsoft.AspNetCore.Http;

namespace Arrba.Admin.ViewModels
{
    public class CategoryCreate : Category
    {
        [Display(Name = "Сделать супер категорией")]
        public IFormFile CategoryFile { get; set; }
    }
}
