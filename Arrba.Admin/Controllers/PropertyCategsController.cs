﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Arrba.Domain;
using Arrba.Domain.Models;
using Microsoft.AspNetCore.Authorization;

namespace Arrba.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PropertyCategsController : Controller
    {
        private readonly DbArrbaContext _context;

        public PropertyCategsController(DbArrbaContext context)
        {
            _context = context;
        }

        // GET: PropertyCategs
        public async Task<IActionResult> Index()
        {
            var dbArrbaContext = _context.PropertyCategs.Include(p => p.Categ).Include(p => p.Property);
            return View(await dbArrbaContext.ToListAsync());
        }

        // GET: PropertyCategs/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var propertyCateg = await _context.PropertyCategs
                .Include(p => p.Categ)
                .Include(p => p.Property)
                .FirstOrDefaultAsync(m => m.PropertyID == id);
            if (propertyCateg == null)
            {
                return NotFound();
            }

            return View(propertyCateg);
        }

        // GET: PropertyCategs/Create
        public IActionResult Create()
        {
            ViewData["CategID"] = new SelectList(_context.Categories, "ID", "Name");
            ViewData["PropertyID"] = new SelectList(_context.Properties, "ID", "Name");
            return View();
        }

        // POST: PropertyCategs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PropertyCateg propertyCateg)
        {
            if (ModelState.IsValid)
            {
                _context.Add(propertyCateg);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategID"] = new SelectList(_context.Categories, "ID", "Name", propertyCateg.CategID);
            ViewData["PropertyID"] = new SelectList(_context.Properties, "ID", "Name", propertyCateg.PropertyID);
            return View(propertyCateg);
        }

        // GET: PropertyCategs/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var propertyCateg = await _context.PropertyCategs.FindAsync(id);
            if (propertyCateg == null)
            {
                return NotFound();
            }
            ViewData["CategID"] = new SelectList(_context.Categories, "ID", "Name", propertyCateg.CategID);
            ViewData["PropertyID"] = new SelectList(_context.Properties, "ID", "Name", propertyCateg.PropertyID);
            return View(propertyCateg);
        }

        // POST: PropertyCategs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, PropertyCateg propertyCateg)
        {
            if (id != propertyCateg.PropertyID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(propertyCateg);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PropertyCategExists(propertyCateg.PropertyID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategID"] = new SelectList(_context.Categories, "ID", "Name", propertyCateg.CategID);
            ViewData["PropertyID"] = new SelectList(_context.Properties, "ID", "Name", propertyCateg.PropertyID);
            return View(propertyCateg);
        }

        // GET: PropertyCategs/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var propertyCateg = await _context.PropertyCategs
                .Include(p => p.Categ)
                .Include(p => p.Property)
                .FirstOrDefaultAsync(m => m.PropertyID == id);
            if (propertyCateg == null)
            {
                return NotFound();
            }

            return View(propertyCateg);
        }

        // POST: PropertyCategs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var propertyCateg = await _context.PropertyCategs.FindAsync(id);
            _context.PropertyCategs.Remove(propertyCateg);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PropertyCategExists(long id)
        {
            return _context.PropertyCategs.Any(e => e.PropertyID == id);
        }
    }
}
