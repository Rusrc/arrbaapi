﻿namespace Arrba.Services.Decline
{
    // TODO POC
    public interface IDecline
    {
        string Nominative { get; }
        string Prepositional { get; }
    }
}