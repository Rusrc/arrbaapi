﻿namespace Arrba.Services.Decline
{
    // TODO POC
    public interface IQuantityCategory
    {
        IDecline Plural { get; }
        IDecline Singular { get; }
    }
}