﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arrba.Domain.Migrations
{
    public partial class AddIsDeliverableFlag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeliverable",
                table: "AdVehicles",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeliverable",
                table: "AdVehicles");
        }
    }
}
