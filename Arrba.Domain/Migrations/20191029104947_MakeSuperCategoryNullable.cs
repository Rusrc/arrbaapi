﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arrba.Domain.Migrations
{
    public partial class MakeSuperCategoryNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdVehicles_SuperCategories_SuperCategID",
                table: "AdVehicles");

            migrationBuilder.DropForeignKey(
                name: "FK_Categories_SuperCategories_SuperCategID",
                table: "Categories");

            migrationBuilder.AlterColumn<long>(
                name: "SuperCategID",
                table: "Categories",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "SuperCategID",
                table: "AdVehicles",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "ConcurrencyStamp",
                value: "3810842a-6c57-4ff8-afd2-64f01a545a64");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "LastLogin", "RegistrationDate" },
                values: new object[] { new DateTime(2019, 10, 29, 13, 49, 47, 219, DateTimeKind.Local).AddTicks(8921), new DateTime(2019, 10, 29, 13, 49, 47, 219, DateTimeKind.Local).AddTicks(1750) });

            migrationBuilder.AddForeignKey(
                name: "FK_AdVehicles_SuperCategories_SuperCategID",
                table: "AdVehicles",
                column: "SuperCategID",
                principalTable: "SuperCategories",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Categories_SuperCategories_SuperCategID",
                table: "Categories",
                column: "SuperCategID",
                principalTable: "SuperCategories",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdVehicles_SuperCategories_SuperCategID",
                table: "AdVehicles");

            migrationBuilder.DropForeignKey(
                name: "FK_Categories_SuperCategories_SuperCategID",
                table: "Categories");

            migrationBuilder.AlterColumn<long>(
                name: "SuperCategID",
                table: "Categories",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "SuperCategID",
                table: "AdVehicles",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "ConcurrencyStamp",
                value: "c5ceaae3-8e16-4dc2-867c-5a6bdbd40999");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "LastLogin", "RegistrationDate" },
                values: new object[] { new DateTime(2019, 10, 29, 13, 27, 46, 410, DateTimeKind.Local).AddTicks(9744), new DateTime(2019, 10, 29, 13, 27, 46, 410, DateTimeKind.Local).AddTicks(4294) });

            migrationBuilder.AddForeignKey(
                name: "FK_AdVehicles_SuperCategories_SuperCategID",
                table: "AdVehicles",
                column: "SuperCategID",
                principalTable: "SuperCategories",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Categories_SuperCategories_SuperCategID",
                table: "Categories",
                column: "SuperCategID",
                principalTable: "SuperCategories",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
