﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Arrba.Domain.Migrations
{
    public partial class RemoveSuperCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdVehicles_SuperCategories_SuperCategID",
                table: "AdVehicles");

            migrationBuilder.DropForeignKey(
                name: "FK_Categories_SuperCategories_SuperCategID",
                table: "Categories");

            migrationBuilder.DropTable(
                name: "SuperCategories");

            migrationBuilder.DropIndex(
                name: "IX_Categories_SuperCategID",
                table: "Categories");

            migrationBuilder.DropIndex(
                name: "IX_AdVehicles_SuperCategID",
                table: "AdVehicles");

            migrationBuilder.DropColumn(
                name: "SuperCategID",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "SuperCategID",
                table: "AdVehicles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "SuperCategID",
                table: "Categories",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "SuperCategID",
                table: "AdVehicles",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SuperCategories",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Alias = table.Column<string>(maxLength: 180, nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    NameMultiLangJson = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    SuperCategType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuperCategories", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Categories_SuperCategID",
                table: "Categories",
                column: "SuperCategID");

            migrationBuilder.CreateIndex(
                name: "IX_AdVehicles_SuperCategID",
                table: "AdVehicles",
                column: "SuperCategID");

            migrationBuilder.CreateIndex(
                name: "IX_SuperCategories_Name",
                table: "SuperCategories",
                column: "Name",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AdVehicles_SuperCategories_SuperCategID",
                table: "AdVehicles",
                column: "SuperCategID",
                principalTable: "SuperCategories",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Categories_SuperCategories_SuperCategID",
                table: "Categories",
                column: "SuperCategID",
                principalTable: "SuperCategories",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
