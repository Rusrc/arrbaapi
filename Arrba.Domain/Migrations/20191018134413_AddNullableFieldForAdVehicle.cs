﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arrba.Domain.Migrations
{
    public partial class AddNullableFieldForAdVehicle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdVehicles_Brands_BrandID",
                table: "AdVehicles");

            migrationBuilder.DropForeignKey(
                name: "FK_AdVehicles_Type_TypeID",
                table: "AdVehicles");

            migrationBuilder.AlterColumn<long>(
                name: "TypeID",
                table: "AdVehicles",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "BrandID",
                table: "AdVehicles",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddForeignKey(
                name: "FK_AdVehicles_Brands_BrandID",
                table: "AdVehicles",
                column: "BrandID",
                principalTable: "Brands",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AdVehicles_Type_TypeID",
                table: "AdVehicles",
                column: "TypeID",
                principalTable: "Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdVehicles_Brands_BrandID",
                table: "AdVehicles");

            migrationBuilder.DropForeignKey(
                name: "FK_AdVehicles_Type_TypeID",
                table: "AdVehicles");

            migrationBuilder.AlterColumn<long>(
                name: "TypeID",
                table: "AdVehicles",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "BrandID",
                table: "AdVehicles",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AdVehicles_Brands_BrandID",
                table: "AdVehicles",
                column: "BrandID",
                principalTable: "Brands",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AdVehicles_Type_TypeID",
                table: "AdVehicles",
                column: "TypeID",
                principalTable: "Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
