﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arrba.Domain.Migrations
{
    public partial class MakeIdsForOrderLower : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemOrder_AdVehicles_ItemID",
                table: "ItemOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemOrder_Orders_OrderID",
                table: "ItemOrder");

            migrationBuilder.RenameColumn(
                name: "OrderID",
                table: "ItemOrder",
                newName: "OrderId");

            migrationBuilder.RenameColumn(
                name: "ItemID",
                table: "ItemOrder",
                newName: "ItemId");

            migrationBuilder.RenameIndex(
                name: "IX_ItemOrder_OrderID",
                table: "ItemOrder",
                newName: "IX_ItemOrder_OrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_ItemOrder_AdVehicles_ItemId",
                table: "ItemOrder",
                column: "ItemId",
                principalTable: "AdVehicles",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemOrder_Orders_OrderId",
                table: "ItemOrder",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemOrder_AdVehicles_ItemId",
                table: "ItemOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemOrder_Orders_OrderId",
                table: "ItemOrder");

            migrationBuilder.RenameColumn(
                name: "OrderId",
                table: "ItemOrder",
                newName: "OrderID");

            migrationBuilder.RenameColumn(
                name: "ItemId",
                table: "ItemOrder",
                newName: "ItemID");

            migrationBuilder.RenameIndex(
                name: "IX_ItemOrder_OrderId",
                table: "ItemOrder",
                newName: "IX_ItemOrder_OrderID");

            migrationBuilder.AddForeignKey(
                name: "FK_ItemOrder_AdVehicles_ItemID",
                table: "ItemOrder",
                column: "ItemID",
                principalTable: "AdVehicles",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemOrder_Orders_OrderID",
                table: "ItemOrder",
                column: "OrderID",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
