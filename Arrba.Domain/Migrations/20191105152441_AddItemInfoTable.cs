﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arrba.Domain.Migrations
{
    public partial class AddItemInfoTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ItemInfo",
                columns: table => new
                {
                    ItemInfoId = table.Column<long>(nullable: false),
                    Weight = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemInfo", x => x.ItemInfoId);
                    table.ForeignKey(
                        name: "FK_ItemInfo_AdVehicles_ItemInfoId",
                        column: x => x.ItemInfoId,
                        principalTable: "AdVehicles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.SetNull);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ItemInfo");
        }
    }
}
