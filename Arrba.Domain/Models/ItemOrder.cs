﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arrba.Domain.Models
{
    public class ItemOrder
    {
        public long ItemId { get; set; }
        public long OrderId { get; set; }

        public virtual AdVehicle Item { get; set; }
        public virtual Order Order { get; set; }
    }

    public class ItemOrderConfiguration : IEntityTypeConfiguration<ItemOrder>
    {
        public void Configure(EntityTypeBuilder<ItemOrder> builder)
        {
            builder.HasKey(p => new { p.ItemId, p.OrderId });

            builder
                .HasOne(sc => sc.Item)
                .WithMany(s => s.ItemOrders)
                .HasForeignKey(sc => sc.ItemId);

            builder
                .HasOne(sc => sc.Order)
                .WithMany(s => s.ItemOrders)
                .HasForeignKey(sc => sc.OrderId);
        }
    }
}
