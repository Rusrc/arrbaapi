﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arrba.Domain.Models
{
    public class Order
    {
        public long Id { get; set; }
        public string Number { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
        public DateTime AddDate { get; set; }

        public ICollection<ItemOrder> ItemOrders { get; set; }
    }

    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder
                .Property(p => p.Number)
                .ValueGeneratedOnAddOrUpdate()
                // TODO not working check what's wrong
                .HasComputedColumnSql("concat(EXTRACT(YEAR FROM now()), EXTRACT(MONTH FROM now()), EXTRACT(DAY FROM now()), \'-\', \"ID\")");
        }
    }
}
