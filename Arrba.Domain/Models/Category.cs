﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Arrba.Domain.Models
{
    public class Category : MultiLangPropertyExtension
    {
        public long ID { get; set; }
        public long CategGroupID { get; set; }
        public string Alias { get; set; }
        public long? ParentID { get; set; }

        [Display(Name = "Спрятать модель при подаче")]
        public bool HideModelField { get; set; }
        [Display(Name = "Спрятать год при подаче")]
        public bool HideYearField { get; set; }
        [Display(Name = "Спрятать заголовок при подаче")]
        public bool HideTitleField { get; set; }
        [Display(Name = "Placeholder для заголовка")]
        public string TitlePlaceholder { get; set; }

        [JsonIgnore]
        public virtual CategGroup CategGroup { get; set; }

        [JsonIgnore]
        [ForeignKey("ParentID")]
        public virtual Category Parent { get; set; }

        public virtual ICollection<Category> Childrens { get; set; }

        [JsonIgnore]
        public virtual ICollection<ItemModel> Models { get; set; }

        [JsonIgnore]
        public virtual ICollection<CategType> CategTypes { get; set; }

        [JsonIgnore]
        public virtual ICollection<CategBrand> CategBrands { get; set; }

        [JsonIgnore]
        public virtual ICollection<PropertyCateg> PropertyCategs { get; set; }

        [JsonIgnore]
        public virtual ICollection<AdVehicle> AdVehicles { get; set; }

        [StringLength(120)]
        public override string Name { get; set; }

        public string FileName { get; set; }
        public ActiveStatus Status { get; set; }

        #region Singular
        // TODO remove later in MultiLangPropertyExtension
        [Display(Name = "Singular name Json")]
        public string NameMultiLangSingularJson { get; set; }

        [NotMapped]
        public string NameMultiLangSingular
        {
            get => NameMultiLangSingularJsonObject.Value;
            set
            {
                if (value == null) throw new ArgumentException(nameof(value));
            }
        }

        [NotMapped]
        public NameMultiLangJson NameMultiLangSingularJsonObject => MakeNameMultiLangJson(NameMultiLangSingularJson);
        #endregion
    }
}