﻿using Newtonsoft.Json;

using System.ComponentModel.DataAnnotations;

namespace Arrba.Domain.Models
{
    public class ItemInfo
    {
        public long ItemInfoId { get; set; }
        public int Weight { get; set; }
        public int Quantity { get; set; }
        [Range(0, 100)]
        public double Discount { get; set; }
        [JsonIgnore]
        public virtual AdVehicle Item { get; set; }
    }
}
